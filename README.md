# Photo Gallery

This is a practise project, with a focus to set up a project from the start.
In essence, it's a gallery that displays photos, where the user can add/ delete the photos or view them in fullScreen.

## Tech stack

* Server API using Express with Typescript support
  * MongoDB for DB
  * Unit tests with jest
* UI is built with Svelte + Snowpack with Typescript support

# Development

After cloning:

```
npm install
```

Then run DB in docker:

```
docker-compose up -d db
```

and finally run UI & API:

```
npm run start:ui
```

```
npm run start:api
```

