FROM node:15-alpine as build

WORKDIR /app

COPY package*.json ./
RUN npm ci

COPY public/ ./public
COPY models/ ./models
COPY api/ ./api
COPY ui/ ./ui

COPY snowpack.config.js svelte.config.js tsconfig*.json jest.config.js .eslintrc.js svelte-routing.d.ts ./

RUN npm run build:ui
RUN npm run build:api


FROM node:15-alpine as final

RUN adduser -S user

WORKDIR /app

COPY package*.json ./
RUN npm ci --production

COPY --from=build /app/build .

USER user

CMD node ./api/src

ENV PORT=3000
EXPOSE $PORT