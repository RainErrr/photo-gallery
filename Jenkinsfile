pipeline {
  agent any

  environment {
    APP = "photo-gallery"
    BUILD = "${JOB_NAME.replace('/', '-')}-$BUILD_NUMBER"
    TEST_CONTAINER = "$BUILD-tests"
    TEST_RESULTS = "build/test-results"
  }

  stages {
    stage("Build") {
      steps {
        sh "docker build --target build -t $BUILD ."
      }
    }
    stage("Tests") {
      steps {
        sh "docker run --name $TEST_CONTAINER $BUILD npm test"
      }
    }
    stage("Build final") {
      steps {
        sh 'docker build --target final -t ${APP}_${APP} .'
      }
    }
    stage('Deploy to Demo') {
      steps {
        sh 'docker-compose -f docker-compose.codeborne.yml -p ${APP} up -d --remove-orphans'
      }
    }
  }
  post {
    always {
      sh "rm -fr $TEST_RESULTS && mkdir -p $TEST_RESULTS"
      sh "docker cp $TEST_CONTAINER:/app/$TEST_RESULTS $TEST_RESULTS/.. && docker rm $TEST_CONTAINER || echo 'No container'"
      sh "touch $TEST_RESULTS/*.xml"
      junit "$TEST_RESULTS/*.xml"
    }
  }
}
