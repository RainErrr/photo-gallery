import App from './App.svelte'

const app = startApp()

export default app
// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
function startApp() {
  return new App({target: (document.getElementById('app') as Element)})
}

if (import.meta['hot']) {
    import.meta['hot'].accept()
    import.meta['hot'].dispose(() => app.$destroy())
}