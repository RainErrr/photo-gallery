import Photo from '@models/Photo'
import api from '../Api'

const getPhotos = async (): Promise<Photo[]> => {
  return await api.get('/api/photos') as Photo[]
}

const uploadPhoto = async (file: File, title: string): Promise<Photo> => {
  return await api.postFile('/api/photos/file', {title}, {file}) as Photo
}

const deletePhoto = async (id: string|undefined): Promise<Record<string, unknown>> => {
  return await api.delete(`/api/photos/${id}`)
}

export {getPhotos, uploadPhoto, deletePhoto}
