import {render} from '@testing-library/svelte'
import Modal from './Modal.svelte'
import {tick} from 'svelte'

test('Modal is shown', async () => {
  const {container, component} = render(Modal, {title: 'Title', show: false, flyParams: {duration: 0}})
  expect(container).not.toContainHTML('Title')

  component.$set({show: true})
  await tick()
  expect(container).toContainHTML('Title')

  expect(document.body).toHaveClass('modal-open')
  expect(document.querySelector('body > .modal-backdrop')).toBeInTheDocument()

  component.$set({show: false})
  await tick()
  expect(document.body).not.toHaveClass('modal-open')
  expect((document.querySelector('body > .modal-backdrop') as HTMLElement).style.animation).toBeTruthy()
})

test('body.modal-open is added on show and removed on destroy', () => {
  const {component} = render(Modal, {title: 'Title', show: true, flyParams: {duration: 0}})
  expect(document.body).toHaveClass('modal-open')
  component.$destroy()
  expect(document.body).not.toHaveClass('modal-open')
})
