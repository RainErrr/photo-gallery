import {ObjectId} from 'bson'

export class WithTimestamp {
  createdAt?: string | Date
  updatedAt?: string | Date
}

export type Id = string | ObjectId

export abstract class Entity extends WithTimestamp{
  _id?: Id
}

export type OptionalTimestamps<T extends WithTimestamp> = Omit<T, 'createdAt' | 'updatedAt'> & {
  createdAt?: Date,
  updatedAt?: Date
}