const svelteOptions = require('./svelte.config')

module.exports = {
  globals: {
    svelte: svelteOptions
  },
  preset: 'ts-jest',
  testMatch: [
    '**/*.test.ts'
  ],
  testPathIgnorePatterns: [
    '/node_modules/', '.idea'
  ],
  transform: {
    '\\.svelte$': ['svelte-jester', {preprocess: true}],
    'node_modules/.+\\.js$': 'babel-jest'
  },
  moduleNameMapper: {
    "^@models(.*)$": "<rootDir>/models$1",
  },
  moduleFileExtensions: ['js', 'ts', 'svelte'],
  setupFilesAfterEnv: [
    '@testing-library/jest-dom/extend-expect'
  ],
  reporters: ['default', ['jest-junit', {
    suiteNameTemplate: "{filepath}",
    titleTemplate: "{title}",
    outputDirectory: 'build/test-results',
  }]],
}
