interface Errors {
  [k: string]: string
}

class ServiceError extends Error {
  errors: Errors

  constructor(message: string, errors: Errors) {
    super(message)
    this.errors = errors
    Object.setPrototypeOf(this, ServiceError.prototype)
  }
}

export {ServiceError}