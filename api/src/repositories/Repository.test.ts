import {ObjectId} from 'bson'
import Repository from './Repository'
import MockDate from 'mockdate'
import {OptionalId} from 'mongodb'
import {OptionalTimestamps} from '@models/Entity'
import ResolvedValue = jest.ResolvedValue

interface Dummy {
  _id: ObjectId
  foo: string
  createdAt: Date
  updatedAt: Date
}

const db = {
  collection: jest.fn(),
}

const collectionName = 'dummy'
let repository: Repository<Dummy>

beforeEach(() => {
  db.collection.mockReturnValue({
    find: jest.fn(),
    findOne: jest.fn(),
    insertOne: jest.fn(),
  })

  repository = new Repository<Dummy>(db as any, collectionName)
})

describe('create', () => {
  it('inserts object to database and sets timestamps', async () => {
    const now = new Date(2020, 12, 28, 5, 57, 0)
    MockDate.set(now)

    const object: OptionalTimestamps<OptionalId<Dummy>> = {foo: 'bar'}

    const persistedObject = {...object, _id: 'id'}
    const insertResult = {result: {ok: 1}, ops: [persistedObject]}

    db.collection().insertOne.mockResolvedValue(insertResult as ResolvedValue<unknown>)

    const actual = await repository.create(object)
    expect(actual).toEqual(persistedObject)
    expect(repository.collection.insertOne).toHaveBeenCalledWith(object)

    expect(object.createdAt).toEqual(now)
    expect(object.updatedAt).toEqual(now)
  })

  it('throws error if object could not be saved', async () => {
    const object: OptionalId<OptionalTimestamps<Dummy>> = {foo: 'bar'}
    const insertResult = {result: {ok: 0}}
    db.collection().insertOne.mockResolvedValue(insertResult as ResolvedValue<unknown>)

    const actual = repository.create(object)
    await expect(actual).rejects.toThrow('Document could not be saved')
  })
})

describe('all', () => {
  it('returns all documents in given collection', async () => {
    const foundObjects = [
      {id: '1'},
      {id: '2'},
    ]

    db.collection().find.mockReturnValue({
      toArray: jest.fn().mockResolvedValue(foundObjects),
    })

    const actual = await repository.all()
    expect(db.collection().find).toHaveBeenCalled()
    expect(actual).toEqual(foundObjects)
  })
})

describe('find', () => {
  it('returns found document if exists', async () => {
    const id = new ObjectId()
    const foundObject = {_id: id, field: 'bar'}

    db.collection().findOne.mockResolvedValue(foundObject as ResolvedValue<unknown>)
    const actual = await repository.find(id)

    expect(actual).toEqual(foundObject)
  })
})