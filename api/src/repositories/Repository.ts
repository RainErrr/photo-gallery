import {Collection, Db, DeleteWriteOpResultObject, FilterQuery, OptionalId, WithId} from 'mongodb'
import {Entity, OptionalTimestamps} from '@models/Entity'
import {ObjectId} from 'bson'

class Repository<T extends Entity> {
  collection: Collection<T>

  constructor(mongoDb: Db, collectionName: string) {
    this.collection = mongoDb.collection(collectionName)
  }

  public async create(doc: OptionalId<OptionalTimestamps<T>>): Promise<WithId<T>> {
    doc.createdAt = doc.updatedAt = new Date()

    return await this.collection.insertOne(doc as unknown as OptionalId<T>).then(response => {
      if (response.result.ok) {
        return response.ops[0]
      } else {
        throw Error('Document could not be saved')
      }
    })
  }

  public async all(): Promise<T[]> {
    return await this.collection.find().toArray()
  }

  public async find(id: string | ObjectId): Promise<T | null> {
    return await this.collection.findOne({_id: new ObjectId(id)} as FilterQuery<T>)
  }

  public async delete(id: ObjectId): Promise<DeleteWriteOpResultObject> {
    return await this.collection.deleteOne({_id: id} as FilterQuery<T>)
  }
}

export default Repository