import {NextFunction, Request, Response} from 'express'
import {StatusCodes} from 'http-status-codes'
import {ServiceError} from '../../exceptions'
import logger from '../../Logger'

const {BAD_REQUEST} = StatusCodes

const errorHandler = (err: Error, req: Request, res: Response, _: NextFunction) => {
  logger.error(err.message, err)

  if (err instanceof ServiceError) {
    return res.status(BAD_REQUEST).json({
      errors: err.errors
    })
  } else {
    return res.status(BAD_REQUEST).json({
      error: err.message
    })
  }
}

export default errorHandler