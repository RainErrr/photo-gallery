import express, {Express} from 'express'
import fileUpload from 'express-fileupload'
import basicAuth from 'express-basic-auth'
import morgan from 'morgan'

const middleware = (server: Express) => {
  server.use(express.json({limit: '1mb'}))
  server.use(express.urlencoded({extended: true}))

  server.use(fileUpload())

  server.use(morgan(process.env.NODE_ENV === 'development' ? 'dev' : 'common'))

  if (process.env.BASIC_AUTH_USER && process.env.BASIC_AUTH_PWD) {
    server.use(basicAuth({
      users: {[process.env.BASIC_AUTH_USER]: process.env.BASIC_AUTH_PWD},
      challenge: true,
    }))
  }
}

export default middleware