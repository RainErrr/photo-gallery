import {ObjectId} from 'bson'
import PhotoService from './PhotoService'
import Photo from '@models/Photo'
import {mocked} from 'ts-jest/utils'
import {UploadedFile} from 'express-fileupload'

jest.mock('../repositories/Repository')
jest.mock('bson')

const photoRepository = {
  all: jest.fn(),
  create: jest.fn(),
  find: jest.fn(),
  delete: jest.fn()
} as any

const localFileService = {
  upload: jest.fn(),
  delete: jest.fn()
} as any

const photoService = new PhotoService(localFileService, photoRepository)

describe('all', () => {
  it('returns all requested photos', async () => {
    const photos: Photo[] = [
      {
        _id: new ObjectId(),
        createdAt: new Date(2020, 1, 1),
        updatedAt: new Date(2020, 1, 1),
        title: 'Title one',
        url: {
          original: '',
          fullHd: '',
          thumbnail: ''
        },
      },
      {
        _id: new ObjectId(),
        createdAt: new Date(2020, 1, 1),
        updatedAt: new Date(2020, 1, 1),
        title: 'Title two',
        url: {
          original: '',
          fullHd: '',
          thumbnail: ''
        },
      },
    ]

    jest.spyOn(photoRepository, 'all').mockResolvedValue(photos)
    const actual = await photoService.all()

    expect(photoRepository.all).toHaveBeenCalled()
    expect(actual).toEqual(photos)
  })
})

describe('createByFile', () => {
  it('should upload to fileStorage and create a photo in database', async () => {
    const objectId = {toString: jest.fn().mockReturnValue('object-id')}

    mocked(ObjectId).mockImplementation(() => objectId as any)

    const url = {original: 'original', fullHd: 'fullHd', thumbnail: 'thumbnail'}
    photoService.resizeAndUpload = jest.fn().mockReturnValue(url)

    const title = 'Test title'
    const photo = {url, _id: objectId, title}
    photoRepository.create.mockResolvedValue(photo)

    const file = {data: Buffer.from('photo content'), name: 'filename.png',} as UploadedFile
    const actual = await photoService.createByFile(file, title)

    expect(actual).toEqual(photo)
    expect(photoRepository.create).toHaveBeenCalledWith(photo)
  })
})

describe('delete', () => {
  it('deletes a photo', async () => {
    const id = new ObjectId()
    const photo = {url: {original: 'original', fullHd: 'fullHd', thumbnail: 'thumbnail'}, _id: id, title: 'Test title'}
    jest.spyOn(photoRepository, 'find').mockResolvedValue(photo)
    jest.spyOn(photoRepository, 'delete').mockResolvedValue({result: {ok: 1}})

    await photoService.delete(id)

    expect(photoRepository.find).toHaveBeenCalledWith(id)
    expect(localFileService.delete).toHaveBeenCalledWith(id)
    expect(photoRepository.delete).toHaveBeenCalledWith(id)
  })

  it('throws exception if photo is not found', async () => {
    jest.clearAllMocks()
    const id = new ObjectId()

    jest.spyOn(photoRepository, 'find').mockResolvedValue(null)
    jest.spyOn(photoRepository, 'delete')

    await expect(photoService.delete(id)).rejects.toThrow('Photo not found')

    expect(photoRepository.find).toHaveBeenCalledWith(id)
    expect(photoRepository.delete).not.toBeCalled()
  })
})