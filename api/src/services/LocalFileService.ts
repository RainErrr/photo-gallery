import fs from 'fs/promises'
import path from 'path'
import {ObjectId} from 'bson'

export default class LocalFileService {
  async upload(data: Buffer, filePath: string) {
    const fileFullPath = LocalFileService.fullPath(filePath)

    await fs.mkdir(path.dirname(fileFullPath), {recursive: true})
    return fs.writeFile(fileFullPath, data)
  }

  async delete(id: ObjectId) {
    const fileFullPath = LocalFileService.fullPath(id.toString())

    await fs.rm(fileFullPath, {recursive: true})
  }

  private static fullPath(filePath: string): string {
    return `public/photos/${filePath}`
  }
}