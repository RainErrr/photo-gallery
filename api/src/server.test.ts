import request from 'supertest'
import {createTestApp} from '../test/helpers'
import {Express} from 'express'

describe('/', () => {
  const app = createTestApp((app: Express) => app)

  it('returns index.html', async () => {
    const response = await request(app).get('/')
    expect(response.status).toEqual(200)
  })
})