import {Request, Response, Router} from 'express'
import PhotoService from '../services/PhotoService'
import {UploadedFile} from 'express-fileupload'
import {StatusCodes} from 'http-status-codes'
import {ObjectId} from 'bson'

interface PostFile {
  file: UploadedFile
  title: string
}

const {BAD_REQUEST, NO_CONTENT} = StatusCodes

export const photoRoutes = (photoService: PhotoService): Router => {
  const router = Router()

  router.get('/', async (req: Request, res: Response) => {
    const photos = await photoService.all()
    return res.json(photos)
  })

  router.post('/file', async (req: Request, res: Response) => {
    try {
      const {file, title} = {...req.body, ...req.files} as PostFile

      const image = await photoService.createByFile(file, title)
      return res.json({success: true, photo: image})
    } catch (e) {
      return res.status(BAD_REQUEST).json({success: false})
    }
  })

  router.delete('/:id', async (req: Request, res: Response) => {
    const {id} = req.params
    await photoService.delete(new ObjectId(id))
    return res.status(NO_CONTENT).end()
  })

  return router
}
