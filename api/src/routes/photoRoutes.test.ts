import {Express} from 'express'
import request from 'supertest'
import {createTestApp} from '../../test/helpers'
import {photoRoutes} from './photoRoutes'
import ResolvedValue = jest.ResolvedValue
import {ObjectId} from 'bson'

const photoService = {
  all: jest.fn(),
  createByFile: jest.fn(),
  delete: jest.fn()
} as any

describe('/api/photos', () => {
  const app = createTestApp((app: Express) => app.use('/api/photos', photoRoutes(photoService)))

  describe('GET /photos', () => {
    it('returns 200', async () => {
      const photos = [
        {
          id: '1',
          title: 'title one',
          file: {},
        },
        {
          id: '2',
          title: 'title two',
          file: {},
        },
      ] as any[]

      photoService.all.mockResolvedValue(photos as ResolvedValue<unknown>)

      const response = await request(app).get('/api/photos')

      expect(response.status).toEqual(200)
      expect(JSON.parse(response.text)).toEqual(photos)
      expect(photoService.all).toHaveBeenCalled()
    })
  })

  describe('POST /photos/file', () => {
    it('returns 200 and photo url', async () => {
      const filePath = 'api/test/fixtures/image.png'

      photoService.createByFile
        .mockResolvedValue({_id: 'image-id', url: '/local/url'} as ResolvedValue<unknown>)
      const expected = {
        photo: {
          _id: 'image-id',
          url: '/local/url'
        },
        success: true
      }

      const response = await request(app).post('/api/photos/file')
        .field({title: 'title'})
        .attach('file', filePath)

      expect(response.status).toEqual(200)
      expect(JSON.parse(response.text)).toEqual(expected)
      expect(photoService.createByFile).toHaveBeenCalled()

      const uploadedFile = photoService.createByFile.mock.calls[0][0]

      expect(uploadedFile).toBeDefined()
    })

    it('returns {success: false} and 400 when service throws exception', async () => {
      const imagePath = 'api/test/fixtures/image.png'

      photoService.createByFile.mockImplementation(() => {
        throw new Error('Photo service error')
      })

      const expected = {success: false}
      const response = await request(app).post('/api/photos/file')
        .attach('image', imagePath)

      expect(response.status).toEqual(400)
      expect(JSON.parse(response.text)).toEqual(expected)
    })

    it('returns {success: false} and 400 when file is not provided', async () => {
      const response = await request(app).post('/api/photos/file')

      expect(response.status).toEqual(400)
      expect(JSON.parse(response.text)).toEqual({success: false})
      expect(photoService.createByFile).toBeCalled()
    })
  })

  describe('DELETE /photos/:id', () => {
    it('returns 204 if successful', async () => {
      const id = new ObjectId()
      photoService.delete.mockResolvedValue(undefined)

      const response = await request(app).delete(`/api/photos/${id}`)

      expect(response.status).toEqual(204)
      expect(photoService.delete).toHaveBeenCalled()
    })
  })
})