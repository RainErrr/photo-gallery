import express, {Express, Request, Response} from 'express'
import 'express-async-errors'
import path from 'path'
import {databaseConnect} from './server/database'
import Repository from './repositories/Repository'
import PhotoService from './services/PhotoService'
import Photo from '@models/Photo'
import LocalFileService from './services/LocalFileService'
import errorHandler from './server/prepare/errorHandler'
import {photoRoutes} from './routes/photoRoutes'
import middleware from './server/prepare/middleware'
import S3FileService from './services/S3FileService'

const createServer = async (): Promise<Express> => {
  const server = express()

  middleware(server)

  const {db} = await databaseConnect()

  const photoRepository = new Repository<Photo>(db, 'photos')
  const uploadFileService = process.env.NODE_ENV === 'development' ? new LocalFileService() : new S3FileService()
  const photoService = new PhotoService(uploadFileService, photoRepository)

  server.use('/api/photos', photoRoutes(photoService))

  const staticDir = path.join(__dirname, '..', '..', 'public')

  server.use(express.static(staticDir))
  server.get('/*', (reqReq: Request, res: Response) => {
    res.sendFile(staticDir + '/index.html')
  })

  server.use(errorHandler)

  return server
}

export {createServer}
