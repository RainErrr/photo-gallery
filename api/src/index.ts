import {createServer} from './Server'

const port = 3000

createServer().then(app => {
  app.listen(port, () => {
    console.info(`Express server started on port: ${port}`)
  })
}).catch(console.error)

