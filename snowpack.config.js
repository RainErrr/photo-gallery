const svelteIgnore = [
  'a11y-autofocus',
  'a11y-no-onchange',
  'a11y-missing-attribute',
  'a11y-label-has-associated-control'
]

  module.exports = {
  mount: {
    public: '/',
    ui: '/_dist_/ui',
    models: '/_dist_/models'
  },
  alias: {
    '@models': './models'
  },
  exclude: [
    '**/node_modules/**/*',
    '**/*.test.ts',
      '**/_*.scss',
  ],
  plugins: [
    '@snowpack/plugin-typescript',
    '@snowpack/plugin-svelte',
    ['@snowpack/plugin-run-script', {
      cmd: 'sass ui/assets/scss:public/css --no-source-map --style=compressed', watch: 'sass ui/assets/scss:public/css --embed-source-map --watch'
    }],
    ['@snowpack/plugin-run-script', {
      cmd: 'svelte-check --output human --compiler-warnings ' + svelteIgnore.map(i => i + ':ignore').join(','), watch: '$1 --watch', output: 'stream'
    }],
  ],
  installOptions: {
    installTypes: true,
    externalPackage: ['bson']
  },
  buildOptions: {
    out: 'build/public',
    sourceMaps: true
  },
  proxy: {
    '/api': 'http://localhost:3000/api',
  },
  devOptions: {
    open: 'none'
  }
}
